#!/bin/bash

SERVICE_NAME=$1
TARGET_IP=$2
TARGET_DNS=$3
TARGET_USERNAME=$4
TARGET_KEY=$5
PORTS_TOBE_FWD=$6

SERVICE_PATH=/etc/systemd/system/$SERVICE_NAME.service

SERVICE_TYPE=forking
# SERVICE_TYPE=simple
SERVICE_STOP=
SERVICE_PERSISTENCE=no

chmod 600 /home/vagrant/.ssh/$TARGET_KEY/*

SERVICE_COMMAND="/usr/bin/ssh -N -f $PORTS_TOBE_FWD -i /home/vagrant/.ssh/$TARGET_KEY/$TARGET_KEY.pem $TARGET_USERNAME@$TARGET_IP -o \"StrictHostKeyChecking no\""

sudo cp templates/service.template $SERVICE_PATH
sudo sed -i -- "s/\$SERVICE_DESCRIPTION/$SERVICE_NAME/g" $SERVICE_PATH
sudo sed -i -- "s/\$SERVICE_TYPE/$SERVICE_TYPE/g" $SERVICE_PATH
sudo sed -i -- "s|\$SERVICE_START|$SERVICE_COMMAND|g" $SERVICE_PATH
sudo sed -i -- "s/\$SERVICE_STOP/$SERVICE_STOP/g" $SERVICE_PATH
# sudo sed -i -- 's/$SERVICE_USER/'$TARGET_USERNAME'/g' $SERVICE_PATH
sudo sed -i -- "s/\$SERVICE_PERSISTENCE/$SERVICE_PERSISTENCE/g" $SERVICE_PATH

{
    sudo systemctl enable $SERVICE_NAME.service
    sudo systemctl daemon-reload
    sudo systemctl start $SERVICE_NAME.service
} || {
    exit 0
}
