#!/bin/bash

TARGET_IP=$1
TARGET_DNS=$2
COUNTER=$3

RUNONCESCRIPT=/home/vagrant/runonce/runoncescript2

if [ ! -f $RUNONCESCRIPT ]
then
	if [ $COUNTER -eq 3 ]
	then
		#ONCE RUN CODE HERE
		sudo runuser -l  vagrant -c 'touch '$RUNONCESCRIPT
		
	fi	
else
	exit 0
fi

echo -e "$TARGET_IP\t$TARGET_DNS" | sudo tee -a /etc/hosts
