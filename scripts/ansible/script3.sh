#!/bin/bash

TARGET_NAME=$1
COUNTER=$2

RUNONCESCRIPT=/home/vagrant/runonce/runoncescript3

if [ ! -f $RUNONCESCRIPT ]
then
	if [ $COUNTER -eq 3 ]
	then
		#ONCE RUN CODE HERE
		sudo runuser -l  vagrant -c 'touch '$RUNONCESCRIPT
	fi	
else
	exit 0
fi

chmod -R 755 /home/vagrant/.ssh

sshpass -p vagrant ssh-copy-id -o StrictHostKeyChecking=no vagrant@$TARGET_NAME

chmod -R 600 /home/vagrant/.ssh/authorized_keys
chmod -R 600 /home/vagrant/.ssh/id_rsa
chmod -R 644 /home/vagrant/.ssh/id_rsa.pub

# ssh vagrant@$TARGET_NAME << EOF
# 	sudo sed -i -- 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
# EOF
