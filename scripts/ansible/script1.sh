#!/bin/bash

sudo mkdir /home/vagrant/runonce
sudo chown -R vagrant:vagrant runonce

RUNONCESCRIPT=/home/vagrant/runonce/runoncescript1

if [ ! -f $RUNONCESCRIPT ]
then
        #ONCE RUN CODE HERE
        sudo runuser -l  vagrant -c 'touch '$RUNONCESCRIPT
else
        exit 0
fi

sudo yum -y install sshpass

yes y | ssh-keygen -t rsa -b 4096 -C "" -N "" -f /home/vagrant/.ssh/id_rsa
