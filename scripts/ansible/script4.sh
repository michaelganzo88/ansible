#!/bin/bash

RUNONCESCRIPT=/home/vagrant/runonce/runoncescript4

if [ ! -f $RUNONCESCRIPT ]
then
	#ONCE RUN CODE HERE
	sudo runuser -l  vagrant -c 'touch '$RUNONCESCRIPT
else
	exit 0
fi

yum -y install wget

mkdir ~/downloads
cd ~/downloads
# yum -y install epel-release

EPEL_REPO=epel-release-latest-7.noarch.rpm

if [ ! -f $EPEL_REPO ]; then
	wget http://dl.fedoraproject.org/pub/epel/$EPEL_REPO
	rpm -ivh $EPEL_REPO
	rm -rf $EPEL_REPO
fi

yum -y install vim
yum -y install python-dev python-pip
# yum pip install ansible
pip install --upgrade pip
yum -y install PyYAML
yum -y install python-paramiko
yum -y install python-jinja2
yum -y install python-crypto
yum -y install python-httplib2
yum -y install python-keyczar
yum -y install python2-jmespath

yum -y install git

ANSIBLE_PKG=ansible-2.5.2-1.el7.noarch.rpm

if [ ! -f $ANSIBLE_PKG ]; then
	wget http://cbs.centos.org/kojifiles/packages/ansible/2.5.2/1.el7/noarch/$ANSIBLE_PKG
	rpm -i $ANSIBLE_PKG
	rm -rf $ANSIBLE_PKG
fi
