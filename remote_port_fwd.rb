#!/usr/bin/ruby

class RemoteNode

	attr_accessor :hostname, :service_name, :ip, :username, :key_path, :ports

	def initialize(hostname, service_name, ip, username, key_path, ports)
		@hostname = hostname
		@service_name = service_name
		@ip = ip
		@username = username
		@key_path = key_path
		@ports = ports
	end

	def display_details()
	  puts "RemoteNode hostname #@hostname"
	  puts "RemoteNode service_name #@service_name"
	  puts "RemoteNode ip #@ip"
	  puts "RemoteNode username #@username"
	  puts "RemoteNode key_path #@key_path"
      puts "RemoteNode ports #@ports"
   	end
	
	def get_ports()
		ports_str = []
		@ports.each {|port_tuple|
			ports_str.push("-L0.0.0.0:#{port_tuple[0]}:127.0.0.1:#{port_tuple[1]}")
		}
		return ports_str.reject(&:empty?).join(' ')
	end
end
