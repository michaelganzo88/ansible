
require './node'
require './remote_port_fwd'

NODES = {
	'vmfrontend' => {
		'os' => 'centos/7',
		'ip' => '192.168.33.11',
		'ports_fwd' => [
			[1443, 14430],
		]
	},
	'vmbackend' => {
		'os' => 'centos/7',
		'ip' => '192.168.33.12',
		'ports_fwd' => []
	},
	'vmdb' => {
		'os' => 'centos/7',
		'ip' => '192.168.33.13',
		'ports_fwd' => [
			[3306, 33060],
		]
	},
}

REPOS = {
	'devkar' => {
		'service_name' => 'aws_tunnel',
		'ip' => '18.232.180.148',
		'username' => 'ec2-user',
		'key' => 'nexi-kar',
		'ports_fwd' => [
			[10503, 10503],
		]
	},
}

$clients_script = <<-SCRIPT

	sed -i -- 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
	systemctl restart sshd

SCRIPT

$create_templates_dir = <<-SCRIPT

	sudo yum -y install net-tools.x86_64
	sudo yum -y install wget

	mkdir -p templates

SCRIPT

$ansiblemaster_script = <<-SCRIPT

	TARGET_IP=$1
	TARGET_DNS=$2
	TARGET_KEY=$3

	echo -e "$TARGET_IP\t$TARGET_DNS" | sudo tee -a /etc/hosts

	chmod 600 /home/vagrant/.ssh/$TARGET_KEY/*

SCRIPT

nodes_obj = []

NODES.each do |node_obj|
	node_name = node_obj[0]
	node_properties = node_obj[1]
	node_os = node_properties['os']
	node_ip = node_properties['ip']
	node_ports_fwd = node_properties['ports_fwd']

	nodes_obj.push(Node.new(node_name, node_os, node_ip, node_ports_fwd))
end

remote_nodes_obj = []

REPOS.each do |remote_node_obj|
	remote_node_name = remote_node_obj[0]
	remote_node_properties = remote_node_obj[1]
	remote_node_service_name = remote_node_properties['service_name']
	remote_node_ip = remote_node_properties['ip']
	remote_node_username = remote_node_properties['username']
	remote_node_key = remote_node_properties['key']
	remote_node_ports_fwd = remote_node_properties['ports_fwd']

	remote_nodes_obj.push(RemoteNode.new(remote_node_name, remote_node_service_name, remote_node_ip, remote_node_username, remote_node_key, remote_node_ports_fwd))
end

Vagrant.configure(2) do |config|

	config.vm.provider "virtualbox" do |v|
		v.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]
	end

	nodes_obj.each do |node_obj|
		config.vm.define node_obj.name.to_sym do |node_config|

			node_config.vm.box = node_obj.os.to_s
			node_config.vm.network :private_network, :ip => node_obj.ip
			
			node_obj.ports.each { |node_port_fwd|
				node_config.vm.network :forwarded_port, guest: node_port_fwd[0], host: node_port_fwd[1]
			}

			node_config.vm.hostname = node_obj.name.to_s
			node_config.vm.provision :shell, inline: $clients_script
			node_config.vm.provision :shell, privileged: false, inline: $create_templates_dir

			# node_config.vm.synced_folder "../data/", "/media" # , type: "nfs"

			remote_nodes_obj.each do |repo_obj|
				
				node_config.vm.provision "file", source: "../keys/#{repo_obj.key_path}", destination: ".ssh/"
				node_config.vm.provision "file", source: "scripts/templates/service.template", destination: "~/templates/service.template"

				ports_tobe_fwd = repo_obj.get_ports()
				puts ports_tobe_fwd

				node_config.vm.provision :shell do |s|
					s.path = "scripts/clients/ssh_tunnel.sh"
					s.args = "#{repo_obj.service_name} #{repo_obj.ip} #{repo_obj.hostname} #{repo_obj.username} #{repo_obj.key_path} #{ports_tobe_fwd}"
				end
				
			end

		end
	end

	# host ansible environment
	config.vm.define "ansiblehost" do |ansible|
		ansible.vm.box = "centos/7"
		ansible.vm.network :private_network, ip: "192.168.33.10"
		ansible.vm.hostname = "ansible"

		# ansible.vm.synced_folder "../data/", "/media" # , type: "nfs"
		ansible.vm.synced_folder "../shared/", "/home/vagrant/shared" # , type: "nfs"

		remote_nodes_obj.each do |repo_obj|
			ansible.vm.provision "file", source: "../keys/#{repo_obj.key_path}", destination: ".ssh/"
			ansible.vm.provision :shell do |s|
				s.inline = $ansiblemaster_script
				s.args = "#{repo_obj.ip} #{repo_obj.hostname} #{repo_obj.key_path}"
			end
		end

		ansible.vm.provision :shell, privileged: false, path: "scripts/ansible/script1.sh"

		node_counter = 0

		nodes_obj.each do |node_obj|
			node_counter += 1
			
			ansible.vm.provision :shell do |s|
				s.path = "scripts/ansible/script2.sh"
				s.args = "#{node_obj.ip} #{node_obj.name} #{node_counter}"
			end

			ansible.vm.provision :shell, privileged: false do |s|
				s.path = "scripts/ansible/script3.sh"
				s.args = "#{node_obj.name} #{node_counter}"
			end
		end

		ansible.vm.provision :shell, path: "scripts/ansible/script4.sh"
		# ansible.vm.provision :shell, inline: "echo Second Script"
	end
end
